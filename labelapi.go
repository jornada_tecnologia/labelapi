package labelapi

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/unidoc/unidoc/pdf/model/fonts"

	"github.com/unidoc/unidoc/pdf/creator"
)

type LabelParameters struct {
	PageSize        creator.PageSize
	TopMargin       float64
	SideMargins     float64
	VerticalPitch   float64
	HorizontalPitch float64
	LabelHeight     float64
	LabelWidth      float64
	Indent          float64
	LabelsPerRow    int
}

var PimacoA4249 = &LabelParameters{
	PageSize:        creator.PageSizeA4,
	TopMargin:       13.5 * creator.PPMM,
	SideMargins:     8 * creator.PPMM,
	VerticalPitch:   15 * creator.PPMM,
	HorizontalPitch: 28 * creator.PPMM,
	LabelHeight:     15 * creator.PPMM,
	LabelWidth:      26 * creator.PPMM,
	Indent:          2 * creator.PPMM,
	LabelsPerRow:    7,
}

type LabelTemplate map[int]struct {
	Font     fonts.Font
	FontSize float64
	Height   float64
}

type Label []string

func Print(params *LabelParameters, template LabelTemplate, labels []Label, firstLabelRow, firstLabelCol int) ([]byte, error) {
	doc := creator.New()
	doc.SetPageSize(params.PageSize)
	doc.SetPageMargins(params.SideMargins, params.SideMargins, params.TopMargin, params.TopMargin)
	columnCount := params.LabelsPerRow*2 - 1
	table := creator.NewTable(columnCount)

	columnWidthFractions := calculateColumnWidthFractions(params)

	if err := table.SetColumnWidths(columnWidthFractions...); err != nil {
		return nil, fmt.Errorf("error setting column widths: %s", err)
	}

	remainingLabels := len(labels)

	var (
		firstLabelInRowIdx    int
		lastLabelInRowIdx     int
		lastLabelIdxIncrement int
		labelRowIdx           int
	)

	for remainingLabels > 0 {

		if labelRowIdx == 0 {
			firstLabelInRowIdx = len(labels) - remainingLabels
			lastLabelIdxIncrement = min(params.LabelsPerRow-firstLabelCol, remainingLabels-1)
			lastLabelInRowIdx = firstLabelInRowIdx + lastLabelIdxIncrement
		}

		for i := firstLabelInRowIdx; i <= lastLabelInRowIdx; i++ {
			if i == 0 {
				if labelRowIdx == 0 {
					skipValue := columnCount * len(template) * (firstLabelRow - 1)
					table.SkipCells(skipValue)
				}
				table.SkipCells(firstLabelCol*2 - 2)
			}
			cell := table.NewCell()
			label := labels[i]
			p := creator.NewParagraph(label[labelRowIdx])
			p.SetFont(template[labelRowIdx].Font)
			p.SetFontSize(template[labelRowIdx].FontSize)
			cell.SetContent(p)
			cell.SetIndent(params.Indent)
			p.SetEnableWrap(true)
			if table.CurCol()%2 != 0 && table.CurCol() < columnCount {
				table.SkipCells(1)
			}
			lastLabelBeforeFinalColumn := i == len(labels)-1 && table.CurCol()+1 < columnCount
			if lastLabelBeforeFinalColumn {
				table.SkipCells(columnCount - table.CurCol())
			}
		}
		if table.CurRow()%len(template) == 0 {
			firstLabelCol = 1
		}

		if labelRowIdx++; labelRowIdx >= len(template) {
			labelRowIdx = 0
			remainingLabels -= lastLabelInRowIdx - firstLabelInRowIdx + 1
		}

	}

	for i := 1; i <= table.CurRow(); i++ {
		var templateIdx int
		if mod := i % len(template); mod != 0 {
			templateIdx = mod - 1
		} else {
			templateIdx = len(template) - 1
		}
		table.SetRowHeight(i, template[templateIdx].Height*params.LabelHeight)
	}

	doc.Draw(table)
	f, err := ioutil.TempFile("", "label")
	if err != nil {
		return nil, err
	}
	defer f.Close()
	if err := doc.Write(f); err != nil {
		return nil, err
	}
	if _, err := f.Seek(0, 0); err != nil {
		return nil, err
	}
	os.Remove(f.Name())
	pdf, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	return pdf, nil
}

func calculateColumnWidthFractions(params *LabelParameters) []float64 {
	rowWidth := params.PageSize[0] - 2*params.SideMargins
	spacerColumnWidth := params.HorizontalPitch - params.LabelWidth
	columnWidths := []float64{}
	for i := 1; i <= params.LabelsPerRow*2-1; i++ {
		var width float64
		if i%2 != 0 {
			width = params.LabelWidth
		} else {
			width = spacerColumnWidth
		}
		columnWidths = append(columnWidths, width/rowWidth)
	}
	return columnWidths
}

func min(x, y int) int {
	if x < y {
		return x
	}

	return y
}
