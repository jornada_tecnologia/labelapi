package labelapi_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"bitbucket.org/jornadatec/labelapi"
	"github.com/unidoc/unidoc/pdf/model/fonts"
)

type PBLabel struct {
	ProductNumber string
	Price         float64
}

func TestCreatePBLabel(t *testing.T) {
	pbLabels := []PBLabel{
		{"PB5001", 35.0},
		{"PB5001", 35.0},
		{"PB5001", 35.0},
		{"PB5001", 35.0},
		{"PB5001", 35.0},
		{"PB5001", 35.0},
		{"PB5001", 35.0},
	}

	var pbLabelTemplate = labelapi.LabelTemplate{
		0: {
			Font:     fonts.NewFontHelvetica(),
			FontSize: 8,
			Height:   3.0 / 5,
		},
		1: {
			Font:     fonts.NewFontHelvetica(),
			FontSize: 8,
			Height:   0.8 / 5,
		},
		2: {
			Font:     fonts.NewFontHelveticaBold(),
			FontSize: 8,
			Height:   1.2 / 5,
		},
	}

	var labels []labelapi.Label
	for _, pbLabel := range pbLabels {
		labels = append(labels, createPBLabel(&pbLabel))
	}
	if pdf, err := labelapi.Print(labelapi.PimacoA4249, pbLabelTemplate, labels, 6, 1); err != nil {
		t.Error(err)
	} else if len(pdf) < 1 {
		t.Error("arquivo vazio")
	} else {
		os.Remove("labels.pdf")
		if err := ioutil.WriteFile("labels.pdf", pdf, 0500); err != nil {
			t.Error(err)
		}
	}
}

func createPBLabel(pbLabel *PBLabel) labelapi.Label {
	return labelapi.Label{
		pbLabel.ProductNumber,
		"",
		strings.Replace(fmt.Sprintf("R$ %.2f", pbLabel.Price), ".", ",", -1),
	}
}
